﻿/*
The MIT License (MIT)

Copyright (c) 2014 Gabor Toth

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Microsoft.Phone.Controls;

namespace CounterTextBox
{
    public class CounterTextBox : TextBox
    {
        private string oldText;
        private ScrollViewer contentControl, watermarkContentControl;
        private double extraHeight = 0, defaultMinHeight = 72;
        private TextBlock tb_Header;

        #region DependencyProps
        public static readonly DependencyProperty CountModeProperty = DependencyProperty.Register("CountMode", typeof(CountMode), typeof(CounterTextBox), new PropertyMetadata(CountMode.Increase));
        public CountMode CountMode
        {
            get { return (CountMode)GetValue(CountModeProperty); }
            set { SetValue(CountModeProperty, value); }
        }

        public static readonly DependencyProperty CountProperty = DependencyProperty.Register("Count", typeof(int), typeof(CounterTextBox), new PropertyMetadata(0));
        public int Count
        {
            get { return (int)GetValue(CountProperty); }
            private set { SetValue(CountProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register("Header", typeof(string), typeof(CounterTextBox), new PropertyMetadata(String.Empty));
        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.Register("Watermark", typeof(string), typeof(CounterTextBox), new PropertyMetadata(String.Empty));
        public string Watermark
        {
            get { return (string)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        public static readonly DependencyProperty IsDoubleTapSelectsAllProperty = DependencyProperty.Register("IsDoubleTapSelectsAll", typeof(bool), typeof(CounterTextBox), new PropertyMetadata(true));
        public bool IsDoubleTapSelectsAll
        {
            get { return (bool)GetValue(IsDoubleTapSelectsAllProperty); }
            set { SetValue(IsDoubleTapSelectsAllProperty, value); }
        }
        private void IsDoubleTapSelectsAll_Changed(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            DoubleTap -= CounterTextBox_DoubleTap;

            if (IsDoubleTapSelectsAll)
            {
                DoubleTap += CounterTextBox_DoubleTap;
            }
        }

        #endregion

        public CounterTextBox()
        {
            DefaultStyleKey = typeof(CounterTextBox);

            Loaded += CounterTextBox_Loaded;
            TextChanged += CounterTextBox_TextChanged;

            RegisterPropertyChangedCallback("IsDoubleTapSelectsAll", IsDoubleTapSelectsAll_Changed);
        }

        private void CounterTextBox_Loaded(object sender, RoutedEventArgs e)
        {
            extraHeight = !String.IsNullOrWhiteSpace(Header) ? tb_Header.ActualHeight : 0;
            defaultMinHeight = 72 + extraHeight;

            if (double.IsNaN(MinHeight) || double.IsInfinity(MinHeight))
            {
                MinHeight = defaultMinHeight;
            }
            if (double.IsNaN(Height)) //Height="auto" fucks up the style
            {
                Height = defaultMinHeight;
            }
            if (MaxLength == 0)
            {
                MaxLength = int.MaxValue;
            }

            if (!String.IsNullOrWhiteSpace(Text)) //if has initial text
            {
                switch (CountMode)
                {
                    case CountMode.Increase:
                        Count = Text.Length;
                        break;
                    case CountMode.Decrease:
                        Count = MaxLength - Text.Length;
                        break;
                }

                contentControl.UpdateLayout();
                var height = (contentControl.ExtentHeight + 30 + extraHeight < defaultMinHeight) ? defaultMinHeight : contentControl.ExtentHeight + 30 + extraHeight;
                Height = (double.IsNaN(MaxHeight) || double.IsInfinity(MaxHeight) || height < MaxHeight) ? height : MaxHeight;

                oldText = Text;
            }
            else
            {
                Count = (CountMode == CountMode.Increase) ? 0 : MaxLength;
            }

            watermarkContentControl.Visibility = (String.IsNullOrEmpty(Text)) ? Visibility.Visible : Visibility.Collapsed;
            if (IsDoubleTapSelectsAll)
            {
                DoubleTap += CounterTextBox_DoubleTap;
            }

            LostFocus += CounterTextBox_LostFocus;
        }

        private void CounterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            extraHeight = !String.IsNullOrWhiteSpace(Header) ? tb_Header.ActualHeight : 0;
            defaultMinHeight = 72 + extraHeight;
            watermarkContentControl.Visibility = (String.IsNullOrEmpty(Text)) ? Visibility.Visible : Visibility.Collapsed;

            contentControl.UpdateLayout();

            switch (CountMode)
            {
                case CountMode.Increase:
                    Count = Text.Length;
                    break;
                case CountMode.Decrease:
                    Count = MaxLength - Text.Length;
                    break;
            }

            if (!string.IsNullOrEmpty(oldText))
            {
                //only scroll when adding text at the end of the TB
                if (Text.Trim().StartsWith(oldText.Trim()))
                {
                    contentControl.ScrollToVerticalOffset(ActualHeight);
                }
            }
            else
            {
                contentControl.ScrollToVerticalOffset(ActualHeight);
            }

            oldText = Text;

            var height = (contentControl.ExtentHeight + 30 + extraHeight < defaultMinHeight) ? defaultMinHeight : contentControl.ExtentHeight + 30 + extraHeight;
            Height = (double.IsNaN(MaxHeight) || double.IsInfinity(MaxHeight) || height < MaxHeight) ? height : MaxHeight;
        }

        private void CounterTextBox_DoubleTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SelectAll();
        }

        private void CounterTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            Select(0, 0);
        }

        private void RegisterPropertyChangedCallback(string propertyName, PropertyChangedCallback callback)
        {
            var binding = new Binding(propertyName) { Source = this };
            var prop = DependencyProperty.RegisterAttached("ListenAttached" + propertyName, typeof(object), GetType(), new PropertyMetadata(callback));

            BindingOperations.SetBinding(this, prop, binding);
        }

        private void Unfocus()
        {
            DependencyObject parent = this;

            try
            {
                do
                {
                    parent = VisualTreeHelper.GetParent(parent);
                } while (parent != null && parent as Control == null);

                if (parent != null && parent as Control != null)
                {
                    (parent as Control).Focus();
                }
            }
            catch (Exception){ }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            contentControl = GetTemplateChild("ContentElement") as ScrollViewer;
            watermarkContentControl = GetTemplateChild("WatermarkContentElement") as ScrollViewer;
            tb_Header = GetTemplateChild("Header") as TextBlock;
        }
    }
}